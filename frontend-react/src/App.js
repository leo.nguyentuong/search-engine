import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import './App.css';
import HomePage from './components/HomePage';
import ResultPage from './components/ResultPage';
import BookContent from './components/BookContent';


function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/resultpage" element={<ResultPage />} />
        <Route path="/bookContent" element={<BookContent />} />

      </Routes>
    </Router>
  );
}

export default App;