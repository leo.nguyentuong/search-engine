import React, { useState, useEffect } from "react";
import "./ResultPage.css";
import topImage from "../assets/books-header.jpg";
import { useLocation, useNavigate } from "react-router-dom";
import { BeatLoader } from "react-spinners";

const ResultPage = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const { inputValue: initialInputValue, isAdvancedSearch : initialIsAdvancedSearch, inputSortBy: initialSortBy } = location.state;
  const [isAdvancedSearch, setIsAdvancedSearch] = useState(initialIsAdvancedSearch);
  const [inputValue, setInputValue] = useState(initialInputValue);
  const [results, setResults] = useState([]);
  const [noResult, setNoResult] = useState(false);
  const [searchTerm, setSearchTerm] = useState(initialInputValue);
  const [isLoading, setIsLoading] = useState(false);
  const [sortBy, setSortBy] = useState(initialSortBy); 

  const search = () => {
    setIsLoading(true);
    setSearchTerm(inputValue);
    const body = {
      query: inputValue,
      sortBy: sortBy 
    };
    fetch("http://localhost:5000/search/simple", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    })
      .then((response) => response.json())
      .then((data) => {
        const filteredData = data.filter((book) => book !== null);
        setResults(filteredData);
        if (filteredData.length === 0) {
          setNoResult(true);
        }
        setIsLoading(false);
      })
      .catch((error) => console.error("Error searching:", error.message));
  };

  const advancedSearch = (regEx) => {
    const bodyRegex = {
      regEx: inputValue,
      sortBy: sortBy 
    };
    fetch("http://localhost:5000/search/advanced", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(bodyRegex),
    })
      .then((response) => response.json())
      .then((data) => {
        const filteredData = data.filter((book) => book !== null);
        setResults(filteredData);
        if (filteredData.length === 0) {
          setNoResult(true);
        }
      })
      .catch((error) => console.error("Error searching:", error.message));
  };

  const regExButton = () => {
    setIsAdvancedSearch(!isAdvancedSearch);
  };

  const handleSortChange = (event) => {
    setSortBy(event.target.value); 
  };

  const handleInputChange = (event) => {
    setNoResult(false);
    setInputValue(event.target.value);
  };

  const handleSearch = (event) => {
    if (event.key === "Enter") {
      setSearchTerm(event.target.value);
    }
  };

  useEffect(() => {
    setSortBy(initialSortBy);  
  }, [initialSortBy]);

  useEffect(() => {
    setSearchTerm(inputValue);
  }, [inputValue]);

  useEffect(() => {
    if (!isAdvancedSearch) {
      search(searchTerm); 
    } else {
      advancedSearch(searchTerm); 
    }
  }, [searchTerm, isAdvancedSearch, sortBy]);
  
  return (
    <div className="container">
      <img src={topImage} className="top-image" alt="top" />
      {!isAdvancedSearch && (
        <div className="input-container">
          <input
            type="text"
            value={inputValue}
            onChange={handleInputChange}
            onKeyDown={handleSearch}
            placeholder="Search Book..."
          />

          <select className="select-container" value={sortBy} onChange={handleSortChange}>
            <option value="">sort by</option>
            <option value="tfidf">TF-IDF</option>
            <option value="betweennessScore">Betweenness Score</option>
            <option value="closenessScore">Closeness Score</option>
            <option value="pageRank">Page Rank</option>
          </select>
          
          <button onClick={regExButton}>regEx</button>
        </div>
        
      )}
      {isAdvancedSearch && (
        <div className="input-container">
          <input
            type="text"
            value={searchTerm}
            onChange={handleInputChange}
            onKeyDown={handleSearch}
            placeholder="Search Book (regex)..."
          />
          <select className="select-container" value={sortBy} onChange={handleSortChange}>
            <option value="">sort by</option>
            <option value="tfidf">TF-IDF</option>
            <option value="betweennessScore">Betweenness Score</option>
            <option value="closenessScore">Closeness Score</option>
            <option value="pageRank">Page Rank</option>
          </select>

          <button onClick={regExButton}>regEx</button>
        </div>
      )}
      
      {isLoading ? (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
          <BeatLoader color="#123abc" loading={isLoading} size={20} />
        </div>
      ) : (
        <>
          {results.length > 0 && (
            <ul>
              {results.map((book, index) => {
                const authors = book.book.authors
                  .map((author) => author.name)
                  .join(", ");
                const imageUrl = book.book.formats["image/jpeg"]; 
                return (
                  <li
                    key={index}
                    onClick={() => {
                      navigate("/bookContent", {
                        state: { url: book.book.formats["text/html"] },
                      });
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    {" "}
                    <img src={imageUrl} alt={book.book.title} />
                    <div>
                      <h2>{book.book.title}</h2>
                      <p>{authors}</p>
                    </div>
                  </li>
                );
              })}
            </ul>
          )}
          {noResult && <p className="noresult">No results found</p>}
        </>
      )}
    </div>
  );
};


export default ResultPage;
