import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './HomePage.css';

const HomePage = () => {
    const [isAdvancedSearch, setIsAdvancedSearch] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const navigate = useNavigate();

    const handleInputChange = (event) => {
      setInputValue(event.target.value);
    };

    const handleSearch = (event) => {
        if (event.key === 'Enter') {
          navigate('/resultpage', { state: { inputValue, isAdvancedSearch } });
        } 
    };

    const regExButton = () => {
      setIsAdvancedSearch(!isAdvancedSearch);
    };


    return (
        <div className="homepage">
            <h1 className="search">Search</h1>
            <h1 className="engine">Engine</h1>
            {!isAdvancedSearch && (
              <div className="search-bar">
                <input type="text" value={inputValue} onChange={handleInputChange} onKeyDown={handleSearch} placeholder="Search Book..." />
              </div>
            )}
            {isAdvancedSearch && (
              <div className="search-bar">
                <input type="text" value={inputValue} onChange={handleInputChange} onKeyDown={handleSearch} placeholder="Search Book (regex)..." />
              </div>
            )}
            <button className="button-regex" onClick={regExButton}>regEx</button>
        </div>
    );
};

export default HomePage;
