import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { BeatLoader } from "react-spinners";

const BookContent = () => {
  const location = useLocation();
  const [bookContent, setBookContent] = useState("");
  const [isLoading, setIsLoading] = useState(true); 

  useEffect(() => {
    const fetchBookContent = async () => {
      console.log("Fetching book content from URL:", location.state.url);
      if (location.state) {
        const response = await fetch("http://localhost:5000/search/fetch", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ url: location.state.url }),
        });
        const data = await response.text();
        console.log("Fetched book content:", data);
        setBookContent(data);
        setIsLoading(false); 
      }
    };
  
    fetchBookContent();
  }, [location]);

  return (
    <div>
      <h1 style={{ display: 'flex', justifyContent: 'center' }} > Book Content</h1>
      {isLoading ? (
        <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
          <BeatLoader color="#123abc" loading={isLoading} size={15} />
        </div>
      ) : (
        <div dangerouslySetInnerHTML={{ __html: bookContent }} />
      )}
    </div>
  );
};

export default BookContent;