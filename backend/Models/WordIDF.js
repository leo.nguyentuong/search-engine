const mongoose = require("mongoose");


const WordIDF = new mongoose.Schema({
    word: {
        type: String,
        required: false
    },
    IDF: {
        type: Number,
        required: false
    }
});
    
module.exports = mongoose.model('WordIDF', WordIDF);