const mongoose = require("mongoose");


const wordBook = new mongoose.Schema({
    word: {
        type: String,
        required: false
    },
    occurrence: {
        type: Number,
        required: false
    },
    bookID: {
        type: Number,
        required: false,
    }
});

module.exports = mongoose.model('wordBook', wordBook);