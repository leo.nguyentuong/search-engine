const mongoose = require("mongoose");


const BookCount = new mongoose.Schema({
    bookID: {
        type: Number,
        required: false
    },
    wordCount: {
        type: Number,
        required: false
    }
});
    
module.exports = mongoose.model('BookCount', BookCount);