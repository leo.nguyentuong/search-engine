const mongoose = require("mongoose");


const BookObject = new mongoose.Schema({
    id: {
        type: Number,
        required: false
    },
    title: {
        type: String,
        required: false
    },
    subjects: {
        type: Array,
        required: false
    },
    authors: {
        type: Array,
        required: false
    },
    translators: {
        type: Array,
        required: false
    },
    bookshelves: {
        type: Array,
        required: false
    },
    languages: {
        type: Array,
        required: false
    },
    media_type: {
        type: String,
        required: false
    },
    formats: {
        type: Object,
        required: false
    },
    download_count: {
        type: Number,
        required: false
    }
});
    
module.exports = mongoose.model('BookObject', BookObject);