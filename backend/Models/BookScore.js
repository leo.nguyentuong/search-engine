const mongoose = require("mongoose");


const BookScore = new mongoose.Schema({
    bookID: {
        type: Number,
        required: false
    },
    betweennessScore: {
        type: Number,
        required: false
    },
    closenessScore: {
        type: Number,
        required: false
    },
    pageRank: {
        type: Number,
        required: false
    }
});
    
module.exports = mongoose.model('BookScore', BookScore);