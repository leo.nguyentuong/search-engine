const { _simpleSearch, _advancedSearch } = require("../services/search.js");
const axios = require("axios");

exports.simpleSearch = async (req, res) => {
  try {
    const query = req.body.query;
    const startTime1 = new Date();
    let result;

    if (req.body.sortBy) {
      const sortBy = req.body.sortBy;
      console.log(`(ROUTE) - Simple search for ${query} - sortBy: ${sortBy}`);
      result = await _simpleSearch(query, 1, 32, sortBy);
    } else {
      console.log(`(ROUTE) - Simple search for ${query}`);
      result = await _simpleSearch(query);
    }
    const endTime1 = new Date();

    const elapsedTime1 = (endTime1 - startTime1) / 1000;
    console.log(`(ROUTE) - Simple search for ${query} - Time taken: ${elapsedTime1} s`);
    res.send(Array.from(result));
  } catch (error) {
    console.error("Error while searching:", error);
    res.sendStatus(500);
  }
};

exports.advancedSearch = async (req, res) => {
  try {
    const regEx = req.body.regEx;
    const startTime1 = new Date();
    let result;

    if (req.body.sortBy) {
      const sortBy = req.body.sortBy;
      console.log(`(ROUTE) - Advanced search for ${regEx} - sortBy: ${sortBy}`);
      result = await _advancedSearch(regEx, 1, 32, sortBy);
    } else {
      console.log(`(ROUTE) - Advanced search for ${regEx}`);
      result = await _advancedSearch(regEx);
    }
    const endTime1 = new Date();
    const elapsedTime1 = (endTime1 - startTime1) / 1000;

    console.log(`(ROUTE) - Advanced search for ${regEx} - Time taken: ${elapsedTime1} s`);
    res.send(Array.from(result));
  } catch (error) {
    console.error("Error while searching:", error);
    res.sendStatus(500);
  }
};

exports.fetch = async (req, res) => {
  try {
    const url = req.body.url;
    console.log(`Fetching URL: ${url}`);
    const response = await axios.get(url);
    res.send(response.data);
  } catch (error) {
    console.error("Error while fetching URL:", error);
    res.sendStatus(500);
  }
};
