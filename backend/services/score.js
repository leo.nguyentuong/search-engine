
const mongoose = require('mongoose');
const WordBook = require('../Models/WordBook');
const WordIDF = require('../Models/WordIDF');
const BookCount = require('../Models/BookCount');
const BookScore = require('../Models/BookScore');

const centrality = require('ngraph.centrality');
const pageRank = require('ngraph.pagerank');
const load = require('ngraph.frombinary');
const Graph = require('ngraph.graph');

let graph = load(new Graph(), { cwd: './graphData' });
const labels = require('./graphData/labels.json');
console.log("labels length", labels.length)
for (const label of labels) {
    graph.addNode(label);
}

let startTime = performance.now();
const betweenness = centrality.betweenness(graph);
let endTime = performance.now(); 
console.log("Betweenness computed in",(endTime-startTime)/1000,"seconds");
startTime = performance.now();
const closeness = centrality.closeness(graph);
endTime = performance.now();
console.log("Closeness computed in",(endTime-startTime)/1000,"seconds");
startTime = performance.now();
const rank = pageRank(graph);
endTime = performance.now();
console.log("PageRank computed in",(endTime-startTime)/1000,"seconds");

let scoreArray = [];
graph.forEachNode(node => {
    scoreArray.push({
        bookID: node.id,
        betweennessScore: betweenness[node.id],
        closenessScore: closeness[node.id],
        pageRank: rank[node.id]
    });
});

// Calculate IDF score of every word
async function calculateIDF() {
    // Get the total number of documents
    const totalDocs = await BookCount.countDocuments();

    // Use aggregation to calculate IDF for each word
    const idfScores = await WordBook.aggregate([
        {
            $group: {
                _id: "$word",
                count: { $sum: 1 }
            }
        },
        {
            $project: {
                _id: 1,
                idf: { $log: [{ $divide: [totalDocs, "$count"] }, 10] }
            }
        }
    ]);

    return idfScores;
}

mongoose.connect('mongodb://192.168.186.219:27017')
//mongoose.connect('mongodb+srv://searchEngineAdmin:0l20K93k627ztPmS@search-engine.nqp14g5.mongodb.net/?retryWrites=true&w=majority&appName=Search-Engine')
    //  mongoose.connect('mongodb://192.168.186.219:27017')
    .then(() => console.log('Mongo : Connecté avec succès'))
    .catch((err) => console.log('Mongo : Echec de connexion à la base de données', err.message))

startTime = performance.now();
calculateIDF().then(idfScores => {
    endTime = performance.now();
    console.log("IDF scores computed in",(endTime-startTime)/1000,"seconds", idfScores.length, "words in total");
    idfScoresForMongo = idfScores.map(score => {
        return {
            word: score._id,
            IDF: score.idf
        }
    });
    WordIDF.insertMany(idfScoresForMongo).then(() => {
        BookScore.insertMany(scoreArray).then(() => {
            mongoose.disconnect();
            console.log("IDF, betweenness, closeness and pagerank scores saved in MongoDB");
        });
    });
});

