const neo4j = require('neo4j-driver');
const URI = 'neo4j+s://da313422.databases.neo4j.io'
const USER = 'neo4j'
const PASSWORD = 'GTGELP5CSERaDe3Q0kRLPIT9aw5Ud92M4NKa4KsTkIQ'

const driver = neo4j.driver(URI, neo4j.auth.basic(USER, PASSWORD));


const createGraph = async (neighbors) => {
    const session = driver.session();
    // DELETE ALL NODES 
    await session.run(
        'MATCH (n) DETACH DELETE n',
        {id: book.id, title: book.title, author: book.author}
    );

    for (let i = 0; i < neighbors.length; i++) {
        const book = neighbors[i].book;
        const neighborBooks = neighbors[i].neighbors;

        // Create a node for the book if it doesn't exist
        await session.run(
            'MERGE (b:Book {id: $id}) SET b += {title: $title, author: $author}',
            {id: book.id, title: book.title, author: book.author}
        );

        // Create nodes for the neighbors and relationships to the book
        for (let j = 0; j < neighborBooks.length; j++) {
            const neighbor = neighborBooks[j];

            await session.run(
                `
                MERGE (n:Book {id: $id}) SET n += {title: $title, author: $author}
                WITH n
                MATCH (b:Book {id: $bookId})
                MERGE (b)-[:SIMILAR_TO {jaccardDistance: $distance}]->(n)
                `,
                {id: neighbor.id, title: neighbor.title, author: neighbor.author, bookId: book.id, distance: neighbor.jaccardDistance}
            );
        }
    }
    session.close();
    driver.close();
};


module.exports = createGraph