const { removeStopwords } = require('stopword');
const lemmatizer = require('node-lemmatizer');
const mongoose = require('mongoose');

const wordBookModel = require('../Models/WordBook');
const WordIDFModel = require('../Models/WordIDF');
const BookCountModel = require('../Models/BookCount');
const BookObjectModel = require('../Models/BookObject');

async function sortOccurrences(bookOccurences, wordIDFs) {
    const bookIDs = new Set(bookOccurences.map(book => book.bookID));
    const wordCounts = {};
    (await BookCountModel.find({ bookID: { $in: Array.from(bookIDs) } })).map(book => wordCounts[book.bookID] = book.wordCount);
    let tfidfScores = new Map();

    for (const book of bookOccurences) {
        const wordIDF = wordIDFs[book.word];
        if (!wordIDF) continue;
        const wordCount = wordCounts[book.bookID];
        const TFIDF = book.occurrence * wordIDF / wordCount;
        const oldScoreForThisBook = tfidfScores.get(book.bookID);
        if (oldScoreForThisBook) {
            tfidfScores.set(book.bookID, oldScoreForThisBook + TFIDF);
        } else tfidfScores.set(book.bookID, TFIDF);
    }
    const bookObjects = {};
    (await BookObjectModel.find({ id: { $in: Array.from(bookIDs) } }, { _id: 0 })).map(book => bookObjects[book.id] = book);
    let result = Array.from(bookIDs).map((bookID) => {
        const bookObject = bookObjects[bookID];
        if (!bookObject) return null;
        return {
            _id: bookID,
            book: bookObject,
            score: tfidfScores.get(bookID),
        };
    }).filter(book => book !== null);
    return result;
}

async function _simpleSearch(query, page = 1, limit = 32, sortBy = 'tfidf') {
    let tokens = query.split(/[^a-zA-Z]+/);
    const filteredTokens = removeStopwords(tokens);
    let processedQuery = [];
    for (const token of filteredTokens) {
        const lemma = lemmatizer.only_lemmas(token.toLowerCase());
        if (lemma) processedQuery.push(...lemma);
    }
    console.log("(Fun) - arguments: ", processedQuery, page, limit, sortBy);

    try {
        if (sortBy !== "tfidf") {
            console.log("++++++ sortBy: ", sortBy);
            const result = await wordBookModel.aggregate([
                {
                    $match: { word: { $in: processedQuery } }
                },
                {
                    $lookup: {
                        from: "bookscores",
                        localField: "bookID",
                        foreignField: "bookID",
                        as: "score"
                    }
                },
                {
                    $lookup: {
                        from: "bookobjects",
                        localField: "bookID",
                        foreignField: "id",
                        as: "book"
                    }
                },
                {
                    $unwind: "$score"
                },
                {
                    $unwind: "$book"
                },
                {
                    $group: {
                        _id: "$bookID",
                        score: { $first: `$score.${sortBy}` },
                        book: { $first: "$book" }
                        
                    }
                },
                {
                    $project: {
                        _id: 0,
                        "book.id": 0,
                    }
                },
                {
                    $project: {
                        score: 1,
                        book: 1
                    }
                },
                {
                    $sort: { [`score`]: -1 }
                },
                {
                    $skip: (page - 1) * limit
                },
                {
                    $limit: limit
                }
            ]);
            return result;
        } else {
            console.log("++++++ sortBy: tfidf (default)");
            const bookOccurences = await wordBookModel.find({ word: { $in: processedQuery } });
            const wordIDFs = {};
            (await WordIDFModel.find({ word: { $in: processedQuery } })).map(word => wordIDFs[word.word] = word.IDF);
            start = performance.now();
            const result = (await sortOccurrences(bookOccurences, wordIDFs)).sort((a, b) => b.score - a.score).slice((page - 1) * limit, page * limit);
            return result;
        }
    } catch (err) {
        console.error("ERROR simple search :", err.message);
        throw err;
    }
}

async function _advancedSearch(regExQuery, page = 1, limit = 32, sortBy = 'tfidf') {
    try {
        if (sortBy !== "tfidf") {
            console.log("++++++ sortBy: ", sortBy);
            const result = await wordBookModel.aggregate([
                {
                    $match: { word: { $regex: regExQuery } }
                },
                {
                    $lookup: {
                        from: "bookscores",
                        localField: "bookID",
                        foreignField: "bookID",
                        as: "score"
                    }
                },
                {
                    $lookup: {
                        from: "bookobjects",
                        localField: "bookID",
                        foreignField: "id",
                        as: "book"
                    }
                },
                {
                    $unwind: "$score"
                },
                {
                    $unwind: "$book"
                },
                {
                    $group: {
                        _id: "$bookID",
                        score: { $first: `$score.${sortBy}` },
                        book: { $first: "$book" }
                        
                    }
                },
                {
                    $project: {
                        _id: 0,
                        "book.id": 0,
                    }
                },
                {
                    $project: {
                        score: 1,
                        book: 1
                    }
                },
                {
                    $sort: { [`score`]: -1 }
                },
                {
                    $skip: (page - 1) * limit
                },
                {
                    $limit: limit
                }
            ]);
            return result;
        } else {
            console.log("++++++ sortBy: tfidf (default)");
            const bookOccurences = await wordBookModel.find({ word: { $regex: regExQuery } });
            const wordIDFs = {};
            (await WordIDFModel.find({ word: { $regex: regExQuery } })).map(word => wordIDFs[word.word] = word.IDF);
            const result = (await sortOccurrences(bookOccurences, wordIDFs)).sort((a, b) => b.score - a.score).slice((page - 1) * limit, page * limit);
            return result;
        }
    } catch (err) {
        console.error("ERROR advanced search :", err.message);
        throw err;
    }
}

module.exports = { _simpleSearch, _advancedSearch };