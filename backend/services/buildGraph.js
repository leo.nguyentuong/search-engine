const mongoose = require('mongoose');
let graph = require('ngraph.graph')();
const save = require('ngraph.tobinary');

const wordBookModel = require('../Models/WordBook');

const JACCARD_THRESHHOLD = 0.65;

function addBookNeighbors(book1Map, book2Map, book1Id, book2Id) {
    let markedWords = {};
    let intersection = 0;
    let union = 0;
    for (const word of Object.keys(book1Map)) {
        if (!book2Map[word]) {
            union += book1Map[word];
            continue;
        }
        union += Math.max(book1Map[word], book2Map[word]);
        intersection += Math.min(book1Map[word], book2Map[word]);
        markedWords[word] = true;
    }
    for (const word of Object.keys(book2Map)) {
        if (markedWords[word]) continue;
        union += book2Map[word];
    }
    const jaccardDistance = 1 - (intersection / union);
    console.log("Jaccard distance between", book1Id, "and", book2Id, ":", jaccardDistance);
    if (jaccardDistance < JACCARD_THRESHHOLD) graph.addLink(book1Id, book2Id, jaccardDistance);
}



async function buildGraph() {
    wordBookModel.aggregate([
        {
            $group: {
                _id: "$bookID",
                words: {
                    $push: {
                        word: "$word",
                        occurrence: "$occurrence"
                    }
                }
            }
        }
    ]).then(docs => {
        mongoose.disconnect();
        let markedBooks = [];
        for (const doc1 of docs) {
            graph.addNode(doc1._id);
            book1Map = {};
            doc1.words.forEach(doc => {
                book1Map[doc.word] = doc.occurrence;
            });
            for (const doc2 of docs) {
                if (doc1._id === doc2._id || markedBooks.includes(doc2._id)) continue;
                book2Map = {};
                doc2.words.forEach(doc => {
                    book2Map[doc.word] = doc.occurrence;
                });
                addBookNeighbors(book1Map, book2Map, doc1._id, doc2._id);
            }
            markedBooks.push(doc1._id);
        }
        save(graph, {
                outDir: './graphData', // folder where to save results. '.' by default
            });
        mongoose.disconnect();
    });
}

mongoose.connect('mongodb://192.168.186.219:27017')
//mongoose.connect('mongodb+srv://searchEngineAdmin:0l20K93k627ztPmS@search-engine.nqp14g5.mongodb.net/?retryWrites=true&w=majority&appName=Search-Engine')
    .then(() => { console.log('Mongo : Connecté avec succès'); buildGraph(); })
    .catch((err) => console.log('Mongo : Echec de connexion à la base de données', err.message))

module.exports = { buildGraph };