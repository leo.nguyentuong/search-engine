const axios = require("axios");
const { removeStopwords } = require('stopword');
const lemmatizer = require('node-lemmatizer');
const getBookNeighbors = require("./buildGraph");
const createGraph = require("./neo4j");
const wordBookModel = require('../Models/WordBook');
const WordIDFModel = require('../Models/WordIDF');
const BookCountModel = require('../Models/BookCount');
const BookObjectModel = require('../Models/BookObject');
var mongoose = require('mongoose');


//connect to BD

//mongoose.connect('mongodb+srv://searchEngineAdmin:0l20K93k627ztPmS@search-engine.nqp14g5.mongodb.net/?retryWrites=true&w=majority&appName=Search-Engine')
mongoose.connect('mongodb://192.168.186.219:27017')
    .then(() => console.log('Mongo : Connecté avec succès'))
    .catch(() => console.log('Mongo : Echec de connexion à la base de données'))


const DOCUMENT_COUNT = 100;
let count = 0;
let page = 1;
let scrapedWords = {};

let booksData = {}; // title, author and URL for each books
let BookCountArray=[];
let bookObjectsArray = [];
let wordBookArrayBIG= []; //old booksMap

async function forwardIndex() {
    const startTime2 = new Date(); 
    console.log("Scraping and analyzing ...");
    while (count < DOCUMENT_COUNT) {
        const _ = await axios.get("http://gutendex.com/books/?languages=en&page=" + page).then(async (response) => {

            let books = response.data.results;
            for (const book of books) {
                count++;
                const id = book.id;
                let urlPlainTxt;
                for (const format in book.formats) {
                    if (format.startsWith("text/plain")) {
                        urlPlainTxt = book.formats[format];
                        break;
                    }
                }             
                  // AWAIT SLOWS DOWN THE PROCESS BUT AVOIDS TLS ERRORS
                if (urlPlainTxt) /*await*/axios.get(urlPlainTxt).then(async (response3) => {
                    booksData[id] = {};
                    booksData[id].URL = urlPlainTxt;
                    booksData[id].title = book.title;
                    booksData[id].author = book.authors[0].name;
                    let tokens = response3.data.split(/[^a-zA-Z]+/);
                    const filteredTokens = removeStopwords(tokens);
                    let lemmas = [];
                    
                    for (const token of filteredTokens) {
                        const lemma = lemmatizer.only_lemmas(token.toLowerCase());
                        if (lemma) lemmas.push(lemma[0]);
                    }

                    
                    scrapedWords[id] = lemmas;

                    let wordsMap = new Map();
                    scrapedWords[id].forEach(word => { wordsMap.has(word)? wordsMap.set(word, wordsMap.get(word)+1) : wordsMap.set(word,1);});
            

                    // bookID -> wordsMap
                    let wordCount=scrapedWords[id].length;
                    if(wordCount<10000) throw new Error("wordCount too low ("+wordCount+")");

                    console.log(book.id," - ",book.title, "scraped successfully -", (count));
                    console.log("wordCount : "+wordCount);

                    try {

                        //Collection BookCount ( bookID - wordCount)
                        const BookCount = new BookCountModel({
                            bookID: id,
                            wordCount: wordCount
                        });

                        BookCountArray.push(BookCount);


                        //Collection WordBook ( word - occurence - bookID)
                        let wordBookArray= [];

                        wordsMap.forEach((occurence, word) => {

                            const wordBook = new wordBookModel({
                                word: word,
                                occurrence: occurence,
                                bookID: id
                            });
                            wordBookArray.push(wordBook);
                        });


                        wordBookArrayBIG.push(...wordBookArray);
                        bookObjectsArray.push(new BookObjectModel(book));
                        
                    } catch (error) {
                        
                    }

                }).catch((error) => {
                    console.error("Error scraping book", book.title, error.message);
                    count--;
                });
                if(count>=DOCUMENT_COUNT) break;
                await new Promise(resolve => setTimeout(resolve, 500));
            }

            page++;
        });
    }

    const endTime2 = new Date();    
    const elapsedTime2 = (endTime2 - startTime2) / 1000; 
    console.log(`Scraping and analyzing is done for ${DOCUMENT_COUNT} books - Time taken: ${elapsedTime2} s`);

    //Storing Collection WordBook ( word - occurence - bookID)
    console.log("Storing ...");
    const startTime1 = new Date(); 
    await wordBookModel.insertMany(wordBookArrayBIG)
    .then((docs)=>{console.log("wordBookArray inserted successfully")})
    .catch((err)=>{console.error("Error while inserting wordBookArray: ", err)})

    const endTime1 = new Date(); 
    const elapsedTime1 = (endTime1 - startTime1) / 1000; 
    console.log(`WordBook saved for ${DOCUMENT_COUNT} books - Time taken: ${elapsedTime1} s`);


    //Collection BookCount ( bookID - wordCount)
    await BookCountModel.insertMany(BookCountArray)
        .then((docs)=>{console.log("BookCount inserted successfully")})
        .catch((err)=>{console.error("Error while inserting BookCount: ", err)})

    //Collection BookObject ( bookID - title - author - URL - ...) cf Gutendex.com
    await BookObjectModel.insertMany(bookObjectsArray)
        .then((docs)=>{console.log("BookObject inserted successfully")})
        .catch((err)=>{console.error("Error while inserting BookObject: ", err)})

    // Calcul IDF
    const booksContainingWord = {};

    // Compter le nombre de livres contenant chaque mot
    wordBookArrayBIG.forEach(wordBook => {
        const word = wordBook.word;

        // Si le mot n'a pas encore été rencontré, initialiser le compteur à 1
        if (!booksContainingWord[word]) {
            booksContainingWord[word] = 1;
        } else {
            // Sinon, incrémenter le compteur
            booksContainingWord[word]++;
        }
        if(isNaN(booksContainingWord[word])) console.log("BOOK COUNT FOR WORD",word, "IS NAN")
    });

    // Calculer l'IDF pour chaque mot
    const wordIDFArray=[];
    Object.keys(booksContainingWord).forEach(word => {
        const booksWithWord = booksContainingWord[word];
        const idf = Math.log(DOCUMENT_COUNT / booksWithWord); // Formule IDF
        if(idf === 0) return;

        const wordIDF = new WordIDFModel({
            word: word,
            IDF: idf    
        });
        if(isNaN(booksWithWord)) console.log(word, "booksWithWord IS NAN")
        if(isNaN(idf)) console.log("idf IS NAN")
        else wordIDFArray.push(wordIDF)
    });
    //Storing on BD
    const startTime = performance.now();
    console.log("Taille de wordIDFArray :", wordIDFArray.length);
    await WordIDFModel.insertMany(wordIDFArray)
        .then(docs => {
            console.log('IDF values saved successfully');
        })
        .catch(err => {
            console.error('Error while saving IDF values:', err);
        });

    const endTime = performance.now();
    const elapsedTime = (endTime - startTime) / 1000; // Convertir en secondes
    console.log(`Time taken: ${elapsedTime} seconds`);

    console.log("**************** Fin de script (function)****************");

    mongoose.disconnect()
            .then(() => {
                console.log('Déconnexion de MongoDB réussie');
            })
            .catch((error) => {
                console.error('Erreur lors de la déconnexion de MongoDB:', error);
            });
    return 0;

}

forwardIndex();
