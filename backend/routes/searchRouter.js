const express = require('express');
const { simpleSearch, advancedSearch, fetch} = require("../Controllers/searchController");

const router = express.Router();

router.post('/simple', simpleSearch);
router.post('/advanced', advancedSearch);
router.post('/fetch', fetch);



module.exports = router;