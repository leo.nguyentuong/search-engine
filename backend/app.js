const express = require('express');
const app = express();
const cors = require('cors');
var mongoose = require('mongoose');
const searchRoutes = require('./routes/searchRouter');
var morgan = require('morgan');


app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true })); 


require('dotenv').config()
app.use(cors());

mongoose.connect(process.env.DATABASE)
  .then(() => console.log('Mongo : Connecté avec succès'))
  .catch(() => console.log('Mongo : Echec de connexion à la base de données'))

// Routes
app.use('/search', searchRoutes);


// Start the server
const port = 5000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
